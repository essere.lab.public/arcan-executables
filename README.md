# Arcan Executables

Executable packages of Arcan.


## Getting started

The following instructions explain how to use the Arcan tool on Java projects.

The command that must be used is:

`java -jar Arcan-1.2.1-SNAPSHOT.jar -p my_project_location -out my_output-results_location -all`

The "-all" parameter runs the detection of all the smells, in particular it detects Unstable Dependency, Cyclic Dependency and Hub-Like Dependency. Find further instruction in the _Arcan 1.2.1 Documentation.pdf_ document.


## Publications

* Scholar: [link](https://scholar.google.it/citations?user=4TjFFzAAAAAJ&hl=it)
* DBLP: [link](https://dblp.org/pid/193/4205.html)

## Contacts

Ilaria Pigazzini, University of Milano - Bicocca
email: i.pigazzini@campus.unimib.it
